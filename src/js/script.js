

function portfolioImageFilter() {
  let filterItem = document.querySelectorAll('.filter__item');
  let portfolioColumn = document.querySelectorAll('.portfolio__column');

  filterItem.forEach(item => {
    item.addEventListener('click', selectPortfolioItem)
  });
  function selectPortfolioItem() {
    filterItem.forEach(item => {
      item.classList.remove('filter__item_active')
    });
    this.classList.add('filter__item_active');
    const dataFilter = this.getAttribute('data-filter');
    showPortfolioItem(dataFilter);
    ignoreShowImageFullscreen();
  };
  function showPortfolioItem(dataFilter) {
    portfolioColumn.forEach(item => {
      dataFilter && !item.classList.contains('f_'+ dataFilter) 
        ? item.classList.remove('filter__show') 
        : item.classList.add('filter__show' );
    });
  };
  function ignoreShowImageFullscreen() {
    portfolioColumn.forEach(item => {
      item.classList.contains('filter__show') 
        ? item.classList.remove('img__ignore') 
        : item.classList.add('img__ignore' );
    })
  }
};

portfolioImageFilter();



// $(document).ready( function(){
//   $('.filtet__item').click(function (event) {
//     var i = $(this).data('filter');
//     if (i==1) {
//       $('.portfolio__column').show();
//     } else {
//       $('.portfolio__column').hide();
//       $('.portfolio__column.f_' + i).show();
//     };
//     $('.filter__item').removeClass('active');
//     $(this).addClass('active')
//     return false;  
//   });
// });

